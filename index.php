//komentarz do gita
<?php
interface iDB {
    public function __construct($host, $login, $password, $dbName);

    public function query($query);

    public function getAffectedRows();

    public function getRow();

    public function getAllRows();
}

class DB implements iDB
{
    private $link;
    private $queryResult;

    public function __construct($host, $login, $password, $dbName)
    {
        $this->link = mysqli_connect($host, $login, $password, $dbName);
        if (!$this->link) {
            throw new Exception ('Nie udalo sie polaczyc z baza danych: ' . $dbName);
        }
    }

     public function query($query)
    {
        $this->queryResult = mysqli_query($this->link, $query);

        return (bool)$this->queryResult;
    }

    public function getAffectedRows()
    {
        return mysqli_affected_rows($this->link);
    }

    public function getRow()
    {
        if (!$this->queryResult) {
            throw new Exception('Zapytanie nie zostalo wykonane');
        }

        return mysqli_fetch_assoc($this->queryResult);
    }

    public function getAllRows()
    {
        $results = array();

        while (($row = $this->getRow())) {
            $results[] = $row;
        }

        return $results;
    }

    public function checkProduct($id)
    {
        $query = "SELECT `id`, `name`, `price` FROM `products` WHERE `id` = '".$id."'";
        $this->queryResult = mysqli_query($this->link, $query);
        return true;
    }

    public function addProduct($name,$price)
    {
        $query = "INSERT INTO `products`(`name`, `price`) VALUES ('$name','$price')";
        $this->queryResult = mysqli_query($this->link, $query);
    }

    public function removeProduct($id)
    {
        $query = "DELETE FROM `products` WHERE `id` = $id";
        $this->queryResult = mysqli_query($this->link, $query);
    }

    public function printAll()
    {
        $query = "SELECT * FROM `products`";
        $this->queryResult = mysqli_query($this->link, $query);
        return $this->queryResult;
    }

    public function asd($name){
        return $name;
    }

}

$DB = new DB('localhost','root','','phpcamp_bmoscinski');
/*$DB->checkProduct("1");
$DB->addProduct("asdd","44");
$DB->removeProduct("11");*/


//switch ($_GET['action'])
//{
//    case 'checkProduct':
//        if($DB->checkProduct($_GET['product']))
//        {
//            echo "Znalazło";
//        }
//        break;
//    case 'addProduct':
//        $DB->addProduct($_GET['name'],$_GET['price']);
//        break;
//    case 'removeProduct':
//        $DB->removeProduct($_GET['product']);
//        break;
//}
//$DB->printAll();
//echo "<pre>";
//var_dump($DB->getAllRows());
//$xx = json_encode($DB->getAllRows());
//echo "</pre>";
//echo $xx;
//header('Content-type: text/xml');
//$DB->printAll();
//
////var_dump($DB->getAllRows());
//
//$xml = new SimpleXMLElement('<root/>');
//foreach ($DB->getAllRows() as $item){
//
//    $xml->addChild('item',$item['name']);
//}
////array_walk_recursive($DB->getAllRows(), array ($xml, 'addChild'));
//print $xml->asXML();

$options = array('uri' => 'http://localhost/');
$server = new SoapServer(Null, $options);
$server->setClass('DB');
$server->handle();

//$DB->printAll();
//var_dump($DB->getAllRows());


