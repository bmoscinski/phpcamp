<?php

class Niebawem
{
//    protected $id;
//    protected $name;
//    protected $price;
//    protected $currency;
//    protected $description;
//    protected $images;
//    protected $producer;
//    protected $category;

    /**
     * baseProduct constructor.
     */
    public function __construct()
    {
    }

    public function __set($name,$value)
    {
        $this->$name=$value;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __call($name,$arguments)
    {
        if(preg_match('/^set.*$/',$name))
        {
            $nazwa = substr($name,3);
            if(ctype_upper($nazwa[0]))
            {
                $nazwa=mb_strtolower($nazwa);
                $this->$nazwa=$arguments[0];
            }
        }
        elseif (preg_match('/^get.*$/',$name))
        {
            $nazwa = substr($name,3);
            if(ctype_upper($nazwa[0]))
            {
                $nazwa=mb_strtolower($nazwa);
                return $this->$nazwa;
            }
        }
    }



}